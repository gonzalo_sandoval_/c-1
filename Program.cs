﻿using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using Example;

//https://drive.google.com/file/d/1TkSPttSp4ZhkfDfS9TFKoIrOhNjz6Iaj/view

namespace CSTutoLinq1
{
	class Program
	{
        static Dictionary<char, MethodInfo> menu;
        static NorthwindViewer viewer;

        static void BuildMenu()
        {
            menu = new Dictionary<char, MethodInfo>();
            char c = 'A';

            var methods = (IEnumerable<MethodInfo>)viewer.GetType().GetMethods().Reverse();
            foreach (MethodInfo mi in methods)
            {
                if (mi.GetCustomAttributes(typeof(RunnableMethodAttribute), false).Length > 0)
                {
                    menu.Add(c, mi);
                    c++;
                }
            }
        }

        static void ShowMenu()
        {
            foreach (KeyValuePair<char, MethodInfo> keyValue in menu)
            {
                Console.WriteLine("[{0}] {1}", keyValue.Key, keyValue.Value.Name);
            }
        }

        static bool ProcessMenu()
        {
            Console.WriteLine("--------------------------------");
            ShowMenu();
            Console.WriteLine("[0] -- exit");

            string s;
            char c;
            do
            {
                s = Console.ReadLine();
                c = (s.Length > 0) ? char.ToUpper(s[0]) : ' ';
            } while (c != '0' && !menu.ContainsKey(c));

            if (c == '0')
            {
                return false;
            }
            else
            {
                MethodInfo mi = menu[c];
                mi.Invoke(viewer, new object[] { });
                return true;
            }
        }

        class NorthwindViewer
        {
            DataClasses1DataContext northwind;

            public NorthwindViewer()
            {
                northwind = new DataClasses1DataContext();
            }

            [RunnableMethod]
            public void NumberOfCustomers()
            {
                Console.WriteLine(northwind.Customers.Count());
            }

            [RunnableMethod]
            public void NumberOfCustomersPerCountry()
            {
                var numbersPerCountries =
                    from customer in northwind.Customers
                    group customer by customer.Country into g
                    orderby g.Count() descending
                    select new { Country = g.Key, Count = g.Count() };

                foreach (var nc in numbersPerCountries)
                {
                    Console.WriteLine("{0}: {1} customer(s)", nc.Country, nc.Count);
                }
            }

            [RunnableMethod]
            public void CustomersFromItaly()
            {
                var Italians = northwind.Customers.Where(c => c.Country == "Italy");

                foreach (var italian in Italians)
                {
                    Console.WriteLine("{0,-30} {1,-10} {2,-20}",
                        italian.CompanyName, italian.Country, italian.City);
                }
            }

            [RunnableMethod]
            public void ProductsAvailability()
            {
                var products =
                    from p in northwind.Products
                    select new
                    {
                        p.ProductName,
                        Availability = p.UnitsInStock - p.UnitsOnOrder < 0 ? "Out Of Stock" : "In Stock"
                    };

                foreach (var p in products)
                {
                    Console.WriteLine("{0}: {1}", p.ProductName, p.Availability);
                }
            }

            [RunnableMethod]
            public void DiscountedProducts()
            {
                var orders =
                    from o in northwind.Orders
                    select new
                    {
                        o.OrderID,
                        DiscountedProducts =
                            from od in o.Order_Details
                            where od.Discount > 0.0
                            select new { OrderDetails = od, od.Products.ProductName }
                    };

                foreach (var order in orders)
                {
                    Console.WriteLine("order ID: {0}", order.OrderID);
                    foreach (var od in order.DiscountedProducts)
                    {
                        Console.WriteLine("    product ID: {0}, name: {1}, discount: {2}",
                            od.OrderDetails.ProductID, od.ProductName, od.OrderDetails.Discount);
                    }
                }
            }


            [RunnableMethod]
            public void TotalFreight()
            {
                var totalFreight = northwind.Orders.Sum(o => o.Freight);

                Console.WriteLine("Total freight: {0}", totalFreight);
            }

            [RunnableMethod]
            public void MostExpensiveProductsPerCategory()
            {
                var categories =
                    from p in northwind.Products
                    group p by p.CategoryID into g
                    join c in northwind.Categories on g.Key equals c.CategoryID
                    select new
                    {
                        CategoryID = g.Key,
                        CategoryName = c.CategoryName,
                        MostExpensiveProducts =
                            from p2 in g
                            where p2.UnitPrice == g.Max(p3 => p3.UnitPrice)
                            select p2
                    };

                foreach (var category in categories)
                {
                    Console.WriteLine("Category ID: {0}, name: {1}",
                        category.CategoryID, category.CategoryName);
                    foreach (var product in category.MostExpensiveProducts)
                    {
                        Console.WriteLine("    Product ID: {0}, name: {1}, unit price: {2}",
                            product.ProductID, product.ProductName, product.UnitPrice);
                    }
                }
            }

            [RunnableMethod]
            public void NumberOfOrdersPerCustomer()
            {
                var qq =
                    from order in northwind.Orders
                    group order by order.CustomerID into orders
                    join customer in northwind.Customers on orders.Key equals customer.CustomerID
                    select new
                    {
                        CustomerID = orders.Key,
                        CompanyName = customer.CompanyName,
                        Count = orders.Count()
                    };

                foreach (var q in qq)
                {
                    Console.WriteLine("Customer ID: {0}, company name: {1}, number of orders: {2}",
                        q.CustomerID, q.CompanyName, q.Count);
                }
            }

            [RunnableMethod]
            public void NumberOfOrdersPerCustomer_Alt()
            {
                var qq =
                    from customer in northwind.Customers
                    select new
                    {
                        CustomerID = customer.CustomerID,
                        CompanyName = customer.CompanyName,
                        Count = northwind.Orders.Where(o => o.CustomerID == customer.CustomerID).Count()
                    };

                foreach (var q in qq)
                {
                    Console.WriteLine("Customer ID: {0}, company name: {1}, number of orders: {2}",
                        q.CustomerID, q.CompanyName, q.Count);
                }
            }

            [RunnableMethod]
            public void SqlMethodsLike()
            {
                var names = from c in northwind.Customers
                            where SqlMethods.Like(c.CompanyName, "%a%e%o%")
                            select c.CompanyName;

                foreach (var name in names)
                {
                    Console.WriteLine(name);
                }
            }

            [RunnableMethod]
            public void InsertNewCustomer()
            {
                Console.WriteLine("Before: {0}", northwind.Customers.Count());

                var newCustomer = new Customers
                {
                    CustomerID = "MCSFT",
                    CompanyName = "Microsoft",
                    ContactName = "John Doe",
                    ContactTitle = "Sales Manager",
                    Address = "1 Microsoft Way",
                    City = "Redmond",
                    Region = "WA",
                    PostalCode = "98052",
                    Country = "USA",
                    Phone = "(425) 555-1234",
                    Fax = null
                };
                northwind.Customers.InsertOnSubmit(newCustomer);
                northwind.SubmitChanges();

                Console.WriteLine("After: {0}", northwind.Customers.Count());
            }

            [RunnableMethod]
            public void DeleteCustomer()
            {
                Console.WriteLine("Before: {0}", northwind.Customers.Count());

                northwind.Customers.DeleteAllOnSubmit(
                    northwind.Customers.Where(c => c.CustomerID == "MCSFT"));
                northwind.SubmitChanges();

                Console.WriteLine("After: {0}", northwind.Customers.Count());
            }

            [RunnableMethod]
            public void ModifyCustomer()
            {
                Console.WriteLine("[1]: {0}", northwind.Customers.First().ContactTitle);

                string old = northwind.Customers.First().ContactTitle;
                northwind.Customers.First().ContactTitle = "-- modified --";
                northwind.SubmitChanges();

                northwind.Customers.First().ContactTitle = "-- modified (2) --";
                Console.WriteLine("[2]: {0}", northwind.Customers.First().ContactTitle);

                // clear the cache by recreating the DataContext object
                northwind = new DataClasses1DataContext() { Log = northwind.Log };
                Console.WriteLine("[3]: {0}", northwind.Customers.First().ContactTitle);

                northwind.Customers.First().ContactTitle = old;
                northwind.SubmitChanges();

                Console.WriteLine("[4]: {0}", northwind.Customers.First().ContactTitle);
            }






        }

        class RunnableMethodAttribute : Attribute
        {
        }

        


        static void Main(string[] args)
        {
            viewer = new NorthwindViewer();
            BuildMenu();

            while (ProcessMenu())
            {
            }

            //viewer.NumberOfCustomersPerCountry();
        }

        

    }

		
}
